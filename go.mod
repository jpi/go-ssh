module go.jpi.io/ssh

go 1.16

require (
	github.com/anmitsu/go-shlex v0.0.0-20200514113438-38f4b401e2be // indirect
	github.com/cloudflare/tableflip v1.2.2
	github.com/gliderlabs/ssh v0.3.2
	github.com/pborman/getopt/v2 v2.1.0
	go.sancus.dev/sancus v0.6.1
	golang.org/x/crypto v0.0.0-20210317152858-513c2a44f670
	gopkg.in/gcfg.v1 v1.2.3
	gopkg.in/warnings.v0 v0.1.2 // indirect
)
