package log

import (
	"log"
)

var (
	Printf  = log.Printf
	Println = log.Println
	Fatal   = log.Fatal
)
