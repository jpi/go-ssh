package log

import (
	"fmt"

	"go.sancus.dev/sancus/log"
)

const (
	NInfo log.Variant = log.Lvariants << iota
	NTrace
	NDebug
	NWarning
	NError
)

type Logger = log.Logger

func NewLogger(prefix string, args ...interface{}) *log.Logger {
	if len(args) > 0 {
		prefix = fmt.Sprintf(prefix, args...)
	}

	return log.New(prefix, log.LPID|log.Lrelative|log.Lseconds|log.LUTC|log.Lelapsed).
		SetVariant(NTrace, "T/", log.Lor|log.Lpackage|log.Lfunc|log.Lshortfile|log.Lfileline).
		SetVariant(NDebug, "D/", log.Lor|log.Lpackage|log.Lfunc).
		SetVariant(NWarning, "W/", log.Lor|log.Lpackage).
		SetVariant(NError, "E/", log.Lor|log.Lpackage).
		SetErrorVariant(NError).
		SetDefaultVariant(NInfo).
		EnableVariant(NInfo)
}
