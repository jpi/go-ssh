package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/cloudflare/tableflip"
	"github.com/pborman/getopt/v2"

	"go.jpi.io/ssh"
	"go.jpi.io/ssh/log"
)

func main() {

	// getopt
	c := NewServerConfig()
	getopt.FlagLong(&c.Addr, "listen", 'l', "address to listen for ssh connections")
	getopt.FlagLong(&c.PIDFile, "pid", 'f', "path to PID file")
	getopt.FlagLong(&c.GracefulTimeout, "graceful", 't', "Maximum duration to wait for connections to close")
	getopt.FlagLong(&c.HostKeys, "keys", 'K', "Location of host keys")
	getopt.FlagLong(&c.ConfigIni, "config", 'c', "Users config file")

	if err := getopt.Getopt(nil); err != nil {
		getopt.SetParameters("")

		log.Println(err)
		getopt.Usage()
		os.Exit(1)
	}

	// logger
	c.Logger.EnableVariant(log.NInfo).SetStandard()

	// Graceful restart mode
	upg, err := tableflip.New(tableflip.Options{
		PIDFile: c.PIDFile,
	})
	if err != nil {
		log.Fatal(err)
	}
	defer upg.Stop()

	// prepare server
	s := &ssh.Server{
		Addr:    c.Addr,
		Version: Version(),
	}

	if err := s.AddHostKeyFiles(c.HostKeys); err != nil {
		c.Fatal(err)
	}

	if err := s.SetDefaults(); err != nil {
		c.Fatal(err)
	}

	if err := c.Configure(s); err != nil {
		c.Fatal(err)
	}

	// listen service port
	ln, err := upg.Listen("tcp", s.Addr)
	if err != nil {
		c.Fatal(err)
	}

	s.Logger = c.Logger.New("%s: ", ln.Addr())
	s.Logger.SetDefaultVariant(log.NTrace).EnableVariant(log.NTrace)

	c.Printf("Listening %s", ln.Addr())

	// watch signals
	go func() {
		sig := make(chan os.Signal, 1)
		signal.Notify(sig, syscall.SIGHUP, syscall.SIGUSR2, syscall.SIGINT, syscall.SIGTERM)

		for signum := range sig {

			switch signum {
			case syscall.SIGHUP:
				// reload config
				c.Println("Reloading...")
				if err := c.Reconfigure(s); err != nil {
					c.Errorln("Reload failed:", err)
				}
			case syscall.SIGUSR2:
				// attempt to upgrade
				c.Println("Upgrading...")
				if err := upg.Upgrade(); err != nil {
					c.Errorln("Upgrade failed:", err)
				}
			case syscall.SIGINT, syscall.SIGTERM:
				// terminate
				c.Println("Terminating...")
				upg.Stop()
			}
		}
	}()

	// start servicing
	go func() {
		err := s.Serve(ln)
		if err != nil && err != ssh.ErrServerClosed {
			c.Fatal(err)
		}
	}()

	// notify being ready for service
	if err := upg.Ready(); err != nil {
		c.Fatal(err)
	}
	<-upg.Exit()

	if c.GracefulTimeout > 0 {
		// graceful shutdown timeout
		time.AfterFunc(c.GracefulTimeout, func() {
			c.Error("Graceful shutdown timed out")
			os.Exit(1)
		})
	}

	// Wait for connections to drain.
	s.Shutdown(context.Background())

	c.Println("Done.")
}

func Version() string {
	return fmt.Sprintf("%s@%s",
		"go.jpi.io/ssh/cmd/sshfront",
		ssh.Version)
}
