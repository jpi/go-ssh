package main

import (
	"gopkg.in/gcfg.v1"
)

type Config struct {
	Logger
	User map[string]*UserConfig
}

func (c *Config) LoadFile(fn string) error {
	if err := gcfg.ReadFileInto(c, fn); err != nil {
		return err
	}
	return nil
}

func (c *Config) SetDefaults() error {
	if c.User == nil {
		c.User = make(map[string]*UserConfig)
	}
	return nil
}

func (s *ServerConfig) ParseFile(fn string) (*Config, error) {
	c := &Config{
		Logger: s.Logger,
	}

	if len(fn) > 0 {
		if err := c.LoadFile(fn); err != nil {
			return nil, err
		}
	}
	if err := c.SetDefaults(); err != nil {
		return nil, err
	}
	return c, nil
}
