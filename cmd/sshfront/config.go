package main

import (
	"time"

	"go.jpi.io/ssh"
)

const (
	DefaultAddress         = ":22"
	DefaultPIDFile         = "/tmp/tableflip.pid"
	DefaultGracefulTimeout = 30 * time.Second
	DefaultConfigIni       = "config.ini"
)

type ServerConfig struct {
	Logger
	Addr            string
	PIDFile         string
	GracefulTimeout time.Duration
	HostKeys        string
	ConfigIni       string
}

func NewServerConfig() ServerConfig {
	return ServerConfig{
		Addr:            DefaultAddress,
		PIDFile:         DefaultPIDFile,
		GracefulTimeout: DefaultGracefulTimeout,
		ConfigIni:       DefaultConfigIni,
		Logger:          NewLogger("ssh: "),
	}
}

// process c.ConfigIni and apply it to &ssh.Server
func (c *ServerConfig) Configure(s *ssh.Server) error {
	if _, err := c.ParseFile(c.ConfigIni); err != nil {
		return err
	}
	return nil
}

func (c *ServerConfig) Reconfigure(s *ssh.Server) error {
	if _, err := c.ParseFile(c.ConfigIni); err != nil {
		return err
	}
	return nil
}
