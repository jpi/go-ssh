package main

import (
	"go.jpi.io/ssh/log"

	sancus "go.sancus.dev/sancus/log"
)

type Logger struct {
	logger *sancus.Logger
}

func NewLogger(s string, args ...interface{}) Logger {
	return Logger{
		logger: log.NewLogger(s, args...),
	}
}

func (c Logger) EnableVariant(n sancus.Variant) *sancus.Logger {
	return c.logger.EnableVariant(n)
}

func (c Logger) New(s string, args ...interface{}) *sancus.Logger {
	return c.logger.New(s, args...)
}

//
func (c Logger) Printf(s string, args ...interface{}) {
	c.logger.Outputf2(1, c.logger.DefaultVariant(), "", s, args...)
}

//
func (c Logger) Println(args ...interface{}) {
	c.logger.Outputln2(1, c.logger.DefaultVariant(), "", args...)
}

//
func (c Logger) Errorln(args ...interface{}) {
	c.logger.Outputln2(1, c.logger.ErrorVariant(), "", args...)
}

//
func (c Logger) Error(args ...interface{}) {
	c.logger.Output2(1, c.logger.ErrorVariant(), "", args...)
}

// log error and os.Exit(1)
func (c Logger) Fatal(args ...interface{}) {
	c.logger.OutputFatal2(1, c.logger.ErrorVariant(), "", args...)
}
