package ssh

import (
	"net"

	"github.com/gliderlabs/ssh"
	gossh "golang.org/x/crypto/ssh"

	"go.jpi.io/ssh/config"
)

type contextKey struct {
	Name string
}

var (
	ContextKeyUserData = &contextKey{"user-data"}
)

// get user data from context
func (srv *Server) getUserFromContext(ctx ssh.Context) config.User {
	if ctx != nil {
		u := ctx.Value(ContextKeyUserData)
		if user, ok := u.(config.User); ok {
			return user
		}
	}

	return nil
}

// get user data if allowed from the user remote
func (srv *Server) getUser(ctx ssh.Context) (config.User, error) {
	user, err := srv.Config.GetUserRemote(ctx.User(), ctx.RemoteAddr())
	if err != nil {
		return nil, err
	} else {
		return user, nil
	}
}

// check if remote is allowed to attempt a login
func (srv *Server) connCallback(ctx ssh.Context, conn net.Conn) net.Conn {

	if err := srv.Config.RemoteAllowed(conn.RemoteAddr()); err != nil {
		// bad remote
		srv.Error(err)
		return nil
	}

	// carry on
	return conn
}

// check if a user can authentication with a given passowrd
func (srv *Server) passwordHandler(ctx ssh.Context, password string) bool {
	user, err := srv.getUser(ctx)

	if user != nil {
		if user.CheckPassword(password) {
			ctx.SetValue(ContextKeyUserData, user)
			return true
		}

		err = config.ErrorCredentialsNotAllowed(user.Username(), "Passowrd")
	}

	srv.Errorln(ctx, err)
	return false
}

// check if a user can authentication with a given challenge-response
func (srv *Server) challengeResponseHandler(ctx ssh.Context, challenger gossh.KeyboardInteractiveChallenge) bool {
	user, err := srv.getUser(ctx)

	if user != nil {
		if user.CheckChallenge(challenger) {
			ctx.SetValue(ContextKeyUserData, user)
			return true
		}

		err = config.ErrorCredentialsNotAllowed(user.Username(), "Challenge")
	}

	srv.Errorln(ctx, err)
	return false
}

// check if a user can authentication with a given key
func (srv *Server) publicKeyHandler(ctx ssh.Context, pk ssh.PublicKey) bool {
	user, err := srv.getUser(ctx)

	if user != nil {
		if user.CheckPublicKey(pk) {
			ctx.SetValue(ContextKeyUserData, user)
			return true
		}

		err = config.ErrorCredentialsNotAllowed(user.Username(), "Key")
	}

	srv.Errorln(ctx, err)
	return false
}
