package config

import (
	"net"
)

type Config struct {
	Allowed []Network
	User    map[string]User
}

func (cfg *Config) GetUserRemote(name string, addr net.Addr) (User, error) {
	if user := cfg.GetUser(name); user != nil {

		if user.RemoteAllowed(addr) {
			return user, nil
		}

	} else {
		// any address
		addr = nil
	}

	return nil, ErrorUserNotAllowed(name, addr)
}
