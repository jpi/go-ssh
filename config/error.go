package config

import (
	"fmt"
	"net"
	"strings"
)

type ErrorNotAllowed struct {
	Username string
	Remote   net.Addr
	Method   string
}

func (e ErrorNotAllowed) Error() string {

	var s []string

	if e.Remote == nil {
		s = append(s, e.Username)
	} else {
		var remote string

		if addr, ok := e.Remote.(*net.TCPAddr); ok {
			remote = addr.IP.String()
		} else {
			remote = fmt.Sprintf("%s (%T)", addr, addr)
		}

		if len(e.Username) == 0 {
			s = append(s, remote)
		} else {
			s = append(s, fmt.Sprintf("%s@%s", e.Username, remote))
		}
	}

	if len(e.Method) > 0 {
		s = append(s, "%s not accepted")
	} else {
		s = append(s, "not allowed")
	}

	return strings.Join(s, " ")
}

func ErrorUserNotAllowed(username string, addr net.Addr) error {
	return ErrorNotAllowed{
		Username: username,
		Remote:   addr,
	}
}

func ErrorCredentialsNotAllowed(username string, method string) error {
	return ErrorNotAllowed{
		Username: username,
		Method:   method,
	}
}
