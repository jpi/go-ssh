package config

import (
	"fmt"
	"net"

	"golang.org/x/crypto/ssh"
)

type User interface {
	Username() string

	// Authentication
	RemoteAllowed(net.Addr) bool                          // User can connect from remote
	CheckPassword(string) bool                            // User can authentication with given password
	CheckChallenge(ssh.KeyboardInteractiveChallenge) bool // User can authentication with given challenge
	CheckPublicKey(ssh.PublicKey) bool                    // User can authentication with given public key
}

func (cfg *Config) GetUser(name string) User {
	// explicitly defined users
	if u, ok := cfg.User[name]; ok {
		return u
	}

	return nil
}

func (cfg *Config) AddUser(user User) error {
	name := user.Username()

	if _, ok := cfg.User[name]; ok {
		return fmt.Errorf("user %q duplicated", name)
	}

	cfg.User[name] = user
	return nil
}
