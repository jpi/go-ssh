package config

import (
	"net"
)

type Network interface {
	Contains(net.Addr) bool
}

// check if a remote is allowed to connect
func (cfg *Config) RemoteAllowed(addr net.Addr) error {
	if len(cfg.Allowed) == 0 {
		// allow any remote
		return nil
	}

	for _, n := range cfg.Allowed {
		if n.Contains(addr) {
			// allowed remote
			return nil
		}
	}

	// remote not allowed
	return ErrorUserNotAllowed("", addr)
}

// add CIDR to list of allowed networks
func (cfg *Config) AllowCIDR(n net.IPNet) {
	cfg.Allowed = append(cfg.Allowed, IPNet{n})
}

// allowed CIDRs
type IPNet struct {
	net net.IPNet
}

func (r IPNet) Contains(addr net.Addr) bool {

	if n, ok := addr.(*net.TCPAddr); ok {
		return r.net.Contains(n.IP)
	}

	return false
}
