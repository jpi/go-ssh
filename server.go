package ssh

import (
	"context"
	"fmt"
	"io/ioutil"
	"net"
	"path/filepath"
	"time"

	"github.com/gliderlabs/ssh"
	crypto "golang.org/x/crypto/ssh"

	"go.sancus.dev/sancus/log"

	"go.jpi.io/ssh/config"
)

var ErrServerClosed = ssh.ErrServerClosed

type Server struct {
	Addr        string        // TCP address to listen on, ":22" if empty
	Version     string        // server version to be sent before the initial handshake
	IdleTimeout time.Duration // connection timeout when no activity, none if empty
	MaxTimeout  time.Duration // absolute connection timeout, none if empty

	PasswordAuthentication          bool // Allow password authentication
	ChallengeResponseAuthentication bool // Allow challenge-response authentication

	Logger *log.Logger
	Config config.Config
	server ssh.Server
}

func NewServer(address string, keys string) (*Server, error) {
	srv := &Server{
		Addr: address,
	}

	if err := srv.AddHostKeyFiles(keys); err != nil {
		return nil, err
	}

	if err := srv.SetDefaults(); err != nil {
		return nil, err
	}

	return srv, nil
}

// HostKeys
func (srv *Server) AddHostKey(hk crypto.Signer) {
	pk := hk.PublicKey()
	srv.Printf(nil, "%s: %s", pk.Type(), crypto.FingerprintLegacyMD5(pk))
	srv.server.AddHostKey(hk)
}

func (srv *Server) AddHostKeyFile(fn string) error {
	key, err := ioutil.ReadFile(fn)
	if err != nil {
		return err
	}

	hk, err := crypto.ParsePrivateKey(key)
	if err != nil {
		return err
	}

	pk := hk.PublicKey()
	srv.Printf(nil, "%s: %s (%s)", pk.Type(), crypto.FingerprintLegacyMD5(pk), fn)

	srv.server.AddHostKey(hk)
	return nil

}

func (srv *Server) AddHostKeyFiles(dir string) error {
	pattern := filepath.Join(dir, "ssh_host_*_key")

	if files, err := filepath.Glob(pattern); err != nil {
		return err
	} else {
		count := 0

		for _, f := range files {
			if err := srv.AddHostKeyFile(f); err != nil {
				srv.Printf(nil, "%s: %s", f, err.Error())
			} else {
				count++
			}
		}

		if count == 0 {
			return fmt.Errorf("%q not found", pattern)
		}

		return nil
	}
}

// Serve
func (srv *Server) Serve(ln net.Listener) error {
	srv.Addr = ln.Addr().String()
	return srv.server.Serve(ln)
}

func (srv *Server) Shutdown(ctx context.Context) error {
	return srv.server.Shutdown(ctx)
}

// ssh.Server
func (srv *Server) SetDefaults() error {
	if len(srv.Addr) == 0 {
		srv.Addr = ":22"
	}

	srv.server = ssh.Server{
		Addr:        srv.Addr,
		Version:     srv.Version,
		IdleTimeout: srv.IdleTimeout,
		MaxTimeout:  srv.MaxTimeout,

		Handler:                       srv.handler,
		PublicKeyHandler:              srv.publicKeyHandler,
		PtyCallback:                   srv.ptyCallback,
		ConnCallback:                  srv.connCallback,
		LocalPortForwardingCallback:   srv.localPortForwardingCallback,
		ReversePortForwardingCallback: srv.reversePortForwardingCallback,
		SessionRequestCallback:        srv.sessionRequestCallback,

		ChannelHandlers:   srv.newChannelHandlers(),
		RequestHandlers:   srv.newRequestHandlers(),
		SubsystemHandlers: srv.newSubsystemHandlers(),
	}

	if srv.PasswordAuthentication {
		srv.server.PasswordHandler = srv.passwordHandler
	}

	if srv.ChallengeResponseAuthentication {
		srv.server.KeyboardInteractiveHandler = srv.challengeResponseHandler
	}

	return nil
}

func (srv *Server) newChannelHandlers() map[string]ssh.ChannelHandler {
	m := make(map[string]ssh.ChannelHandler)

	return m
}

func (srv *Server) newRequestHandlers() map[string]ssh.RequestHandler {
	m := make(map[string]ssh.RequestHandler)

	return m
}

func (srv *Server) newSubsystemHandlers() map[string]ssh.SubsystemHandler {
	m := make(map[string]ssh.SubsystemHandler)

	return m
}

// callbacks
func (srv *Server) localPortForwardingCallback(ctx ssh.Context, destinationHost string, destinationPort uint32) bool {
	srv.Println(ctx, destinationHost, destinationPort)
	return false
}

func (srv *Server) reversePortForwardingCallback(ctx ssh.Context, bindHost string, bindPort uint32) bool {
	srv.Println(ctx, bindHost, bindPort)
	return false
}

func (srv *Server) ptyCallback(ctx ssh.Context, pty ssh.Pty) bool {
	srv.Println(ctx, pty)
	return false
}

// ssh.Session
func (srv *Server) sessionRequestCallback(sess ssh.Session, requestType string) bool {
	srv.Println(sess, requestType)
	return false
}

func (srv *Server) handler(sess ssh.Session) {
	srv.Println(sess)
}
