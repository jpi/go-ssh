package ssh

import (
	"fmt"
	"log"
	"net"

	"github.com/gliderlabs/ssh"
)

func (srv *Server) logPrefix(ctx interface{}) string {
	if ctx == nil {
		return ""
	} else if v, ok := ctx.(fmt.Stringer); ok {
		return v.String()
	} else if v, ok := ctx.(ssh.Context); ok {
		var s string

		if addr, ok := v.RemoteAddr().(*net.TCPAddr); ok {
			s = addr.IP.String()
		} else {
			s = v.RemoteAddr().String()
		}

		return fmt.Sprintf("%s@%s", v.User(), s)
	} else {
		return fmt.Sprintf("%T", ctx)
	}
}

func (srv *Server) Printf(ctx interface{}, s string, args ...interface{}) {
	prefix := srv.logPrefix(ctx)

	if srv.Logger != nil {
		srv.Logger.Outputf2(1, srv.Logger.DefaultVariant(), prefix, s, args...)
	} else {

		if len(prefix) > 0 {
			s = "%s: " + s
			args = append([]interface{}{prefix}, args...)
		}

		log.Printf(s, args...)
	}
}

func (srv *Server) Println(ctx interface{}, args ...interface{}) {
	prefix := srv.logPrefix(ctx)

	if srv.Logger != nil {
		srv.Logger.OutputPrettyln2(1, srv.Logger.DefaultVariant(), prefix, args...)
	} else {
		if len(prefix) > 0 {
			prefix = prefix + ":"
			args = append([]interface{}{prefix}, args...)
		}
		log.Println(args...)
	}
}

func (srv *Server) Errorf(ctx interface{}, s string, args ...interface{}) {
	prefix := srv.logPrefix(ctx)

	if len(prefix) > 0 {
		s = "%s: " + s
		args = append([]interface{}{prefix}, args...)
	}

	if srv.Logger != nil {
		srv.Logger.Errorf(s, args...)
	} else {
		log.Printf(s, args...)
	}
}

func (srv *Server) Errorln(ctx interface{}, args ...interface{}) {
	prefix := srv.logPrefix(ctx)

	if len(prefix) > 0 {
		prefix = prefix + ":"
		args = append([]interface{}{prefix}, args...)
	}

	if srv.Logger != nil {
		srv.Logger.Errorln(args...)
	} else {
		log.Println(args...)
	}
}

func (srv *Server) Error(ctx interface{}, args ...interface{}) {
	prefix := srv.logPrefix(ctx)

	if len(prefix) > 0 {
		prefix = prefix + ":"
		args = append([]interface{}{prefix}, args...)
	}

	if srv.Logger != nil {
		srv.Logger.Error(args...)
	} else {
		log.Print(args...)
	}
}
